# 『東洋クイーン』公式ウェブサイト

* [TOC]

---

## 1. 自己紹介

* 宇宙に伸びるエレベータに乗ってみたいと妄想してます。
* ロケットで行くのもいいけど、これからは静止軌道エレベータですよ。非同期軌道スカイフックや超極音速スカイフックじゃないですよ。
* 宇宙開発のシナリオ『長い長い衛星　構想：空をぶち抜くエレベータを作ってみる』を公開しました。
* 12話中7話までの無料サンプル版もあります。

### 『無重力マネー　ゼロサムゲームの世界でソフトウェア開発』(仮）

* 「ソフトウェア開発が得意な少女たちが開発経験を重ねてより良い経済圏を作っていく」シナリオを作成中です。
* 低成長によるお金の過剰貯蓄が変わる。
* 労働の複雑化で時代遅れになった「奴隷」のように水面下に潜る。
* 潜るのは『金利』。
* 体内の血液で例えると、血が過剰に留まるのでなく、血を体に回すのだ。
* 【金は天下の回りもの】

## 2. もっと自己紹介

### 『長い長い衛星　構想：空をぶち抜くエレベータを作ってみる』

* あらすじ：一般人の宇宙旅行を可能にする技術、静止軌道エレベータ。
    * エレベータの鍵となる技術を持つ女性主人公が宇宙旅行を進めたい資金提供者の男性と出会い、協力してエレベータ建設。
    * ライバルの国際機関も既にエレベータ建設目指して活動中。
    * 主人公側がケーブルを地球につなぎ、静止軌道の新秩序を宣言。十五年のケーブル補強を始める。
    * ライバル側は主導権を取り返そうと監督機関の骨抜きを批判。
    * エネルギー輸出国は宇宙発電を生産調整に追い込み..
* 2015年にシナリオ形式小説を公開しました。
*  [#読み放題](https://twitter.com/search?q=%23読み放題) (全12話まで)
    * KDDI ブックパス・​​https://www.bookpass.auone.jp/title/?match=1&sclmn=author&kwd=%E6%9D%B1%E6%B4%8B%E3%82%AF%E3%82%A4%E3%83%BC%E3%83%B3
    * Kindleストア・・・http://www.amazon.co.jp/s/ref=ntt_athr_dp_sr_1?_encoding=UTF8&field-author=%E6%9D%B1%E6%B4%8B%E3%82%AF%E3%82%A4%E3%83%BC%E3%83%B3&search-alias=digital-text
* 無料サンプルは7話まで
    * 楽天Kobo ・・http://search.books.rakuten.co.jp/bksearch/nm?b=1&g=101&sitem=%C5%EC%CD%CE%A5%AF%A5%A4%A1%BC%A5%F3&x=0&y=0
        * 口コミ ・・http://review.rakuten.co.jp/item/1/278256_15181266/1.1/
* 無料サンプルあり
    * iBooks Store(iOS)・・「東洋クイーン」で検索
    * BOOK☆WALKER・・https://bookwalker.jp/author/56264/%E6%9D%B1%E6%B4%8B%E3%82%AF%E3%82%A4%E3%83%BC%E3%83%B3/
    * eBookJapan・・・・https://ebookjapan.yahoo.co.jp/books/346088/
    * 紀伊国屋書店・・・・https://www.kinokuniya.co.jp/f/dsg-08-EK-0301417
    * BookLive!・・・・・・https://booklive.jp/product/index/title_id/355339/vol_no/001
* その他
    * Monappy(モナーコイン取引所)・​https://monappy.jp/u/toyo_queen
    * Sony Reader Store・​http://ebookstore.sony.jp/search/?q=%E6%9D%B1%E6%B4%8B%E3%82%AF%E3%82%A4%E3%83%BC%E3%83%B3
    * アイコン提供：so3ro＠金河南・[![so3ro＠金河南](so3ro-bana.gif)](http://so3ro.web.fc2.com/)

### 『隕石は何を望む？　「幸運を呼ぶ力」と胡散臭い男と男子高校生』

* 隕石がもたらす『幸運を呼ぶ力』を手に入れる主人公と詐欺師の思惑がからみ、話が宇宙へ展開していく。
    * 主人公が選んだ隕石の意外な使い方をどう思いますか？
    * あなたなら『幸運を呼ぶ力』をどのように使いたいですか？
    * これは小説ではなく脚本。あなたは読者ではなく声優。
    * さあ、物語を始めよう、あなたの演出で。
* 2012年に発表したシナリオ形式小説を手直しした作品を公開しました。
    * (閉鎖)コウタリ・ミックス・・・http://web.archive.org/web/20121221111710/http://coterie-mix.net:80/open/userlist/
* アルファポリスで全3話完結まで公開
    * 作品・・・・・・http://www.alphapolis.co.jp/content/cover/960107801
    * 東洋クイーン・・http://www.alphapolis.co.jp/author/detail/152294965/
* comico（コミコ）で全3話完結まで公開
    * ノベル　チャレンジ作品・・・http://novel.comico.jp/challenge/24348/
    * 作家　　東洋クイーン・・・・http://www.comico.jp/user/2149245425/article/

## 3. 問い合わせ

* ツイッター・・・・・https://twitter.com/toyo_queen
* メールフォーム・・・https://mailform.mface.jp/frms/toyoqueen/mg5aby2fssuy
* ウィキペディア・・・https://ja.wikipedia.org/wiki/%E5%88%A9%E7%94%A8%E8%80%85:Toyo_queen
